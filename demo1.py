from sqlalchemy import Column, String, Integer, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20))

    def __inin__(self, **kw):
        self.__dict__.update(kw)


engine = create_engine('sqlite:///user.db')

# create database
Base.metadata.create_all(engine)

# insert data
DBSession = sessionmaker(bind=engine)
session = DBSession()

user = User(name='wen')
session.add(user)
session.commit()

# select :sql
connection = engine.connect()
result = connection.execute('select name from user')
for row in result:
    print('name', row.name)
connection.close()

# selsect :Orm
read_user = session.query(User).all()
print(read_user)
